import {defineConfig, devices} from "@playwright/test"

export default defineConfig({
    testDir: "./tests",
    projects: [
        {name: "firefox", use: {...devices["Desktop Firefox"]}}
    ],
    webServer: {
        command: "npm run dev",
        url: "http://localhost:5173"
    }
})
