import {expect, test} from "@playwright/test"
import {promises as fs} from "fs"

const pageUrl = new URL("../src/pages/2023-12-10_lorem-ipsum.mdx", import.meta.url)

async function replaceInPage(oldString, newString) {
    const pageContent = await fs.readFile(pageUrl, {encoding: "utf8"})
    const newPageContent = pageContent.replace(oldString, newString)
    await fs.writeFile(pageUrl, newPageContent, {encoding: "utf8"})
}

test("hot update should kinda work", async ({page}) => {
    await page.goto("http://localhost:5173")

    // vite seems to ignore file changes if the file hasn't been "analyzed" yet (i.e. loaded)
    // since our plugin is optimized for lazy-loading, it means that hot-update doesn't work very well in our case
    // (see https://gitlab.com/__mazerty__/rollup-plugin-pages/-/issues/1 for more information)
    // so to be able to actually test the hot update, we need to load the page then come back to the summary
    await page.getByRole("link", {name: "Lorem Ipsum"}).click()
    await page.getByRole("link", {name: "Blog"}).click()

    await expect(page.getByRole("link", {name: "Lorem Ipsum"})).toBeVisible()
    await expect(page.getByRole("link", {name: "Dolor Sit Amet"})).not.toBeVisible()

    // changing the frontmatter in the page to trigger hot update
    await replaceInPage("title: Lorem Ipsum", "title: Dolor Sit Amet")

    await expect(page.getByRole("link", {name: "Lorem Ipsum"})).not.toBeVisible()
    await expect(page.getByRole("link", {name: "Dolor Sit Amet"})).toBeVisible()

    // reverting the change
    await replaceInPage("title: Dolor Sit Amet", "title: Lorem Ipsum")
})
