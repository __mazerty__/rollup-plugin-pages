import {Suspense} from "react"
import {Link, Outlet} from "react-router-dom"
import Loading from "./Loading"

export default () => <>
    <nav>
        <Link to="/">Blog</Link>
    </nav>
    <main>
        <Suspense fallback={<Loading/>}><Outlet/></Suspense>
    </main>
</>
