import {Link} from "react-router-dom"
import pages from "virtual:pages"

export default () => pages.map(page => <div key={page.key}>
    <Link to={page.key}>{page.title}</Link>
</div>)
