import {expect, test} from "vitest"
import pages from "../index.js"

const reference = `
export default [
  {
    "import": () => import("${__dirname}/pages/1970-01-01_lorem-ipsum.md"),
    "date": "1970-01-01",
    "key": "lorem-ipsum",
    "title": "Lorem Ipsum",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
  },
  {
    "import": () => import("${__dirname}/pages/1970-01-02_dolor-sit-amet.md"),
    "date": "1970-01-02",
    "key": "dolor-sit-amet",
    "title": "Dolor Sit Amet",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
  }
]
`.trim()

test("the plugin must generate the code of a valid virtual module", async () => {
    const plugin = pages(__dirname + "/pages")
    const virtualModuleCode = await plugin.load(plugin.resolveId(plugin.name))

    expect(virtualModuleCode).toStrictEqual(reference)
})
