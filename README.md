# rollup-plugin-pages

Rollup/Vite plugin that can be used to extract metadata from a directory of markdown files with _frontmatter_ (such as blog pages).  
It can then be imported like any other module in your existing application to populate a router or a summary page.

## Demo

[rollup-plugin-pages.mazerty.fr](https://rollup-plugin-pages.mazerty.fr) is a React application that has been built around this plugin and features a table of contents and a basic router.  
The source code is available on [GitLab](https://gitlab.com/__mazerty__/rollup-plugin-pages-demo) and can be used to bootstrap your own application.

## Installation

You can add `@__mazerty__/rollup-plugin-pages` in your `devDependencies` yourself, or you can let your favorite package manager do it for you.  
For example:

```
npm install @__mazerty__/rollup-plugin-pages --save-dev
```

## Usage

Let's say you have a project with a file structure that looks something like this:

```
src/
  pages/
    1970-01-01_lorem-ipsum.md
    1970-01-02_dolor-sit-amet.md
tests/
package.json
rollup.config.js or vite.config.js
```

And let's say the frontmatter of your markdown pages looks something like this:

```yaml
---
title: Lorem Ipsum
description: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
---
```

Just add the plugin in your Rollup/Vite configuration file:

```javascript
import {defineConfig} from "vite"
import pages from "@__mazerty__/rollup-plugin-pages"

export default defineConfig({
    plugins: [
        pages(__dirname + "/src/pages")
    ]
})
```

And Rollup/Vite will build a [virtual module](https://vitejs.dev/guide/api-plugin#virtual-modules-convention) that you can import like any other module:

```javascript
import pages from "virtual:pages"
```

The code of the virtual module would look something like this:

```javascript
export default [
    {
        "import": () => import("src/pages/1970-01-01_lorem-ipsum.md"),
        "date": "1970-01-01",
        "key": "lorem-ipsum",
        "title": "Lorem Ipsum",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    },
    {
        "import": () => import("src/pages/1970-01-02_dolor-sit-amet.md"),
        "date": "1970-01-02",
        "key": "dolor-sit-amet",
        "title": "Dolor Sit Amet",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    }
]
```

* The `date` and `key` attributes are extracted from the filename (this is fully customizable through the `filenamePattern` parameter, see below).
* The `title` and `description` attributes come from the frontmatter, which can be any valid [yaml mapping](https://yaml.org/spec/1.2.2/#mapping).
* The `import` attribute is a function that allows you to [dynamically import](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/import) the page into your application
  (see also [lazy loading](https://developer.mozilla.org/en-US/docs/Web/Performance/Lazy_loading) and [code splitting](https://developer.mozilla.org/en-US/docs/Glossary/Code_splitting)).

> You'll still need a library such as [MDX](https://mdxjs.com) or [unplugin-vue-markdown](https://github.com/unplugin/unplugin-vue-markdown) to turn your markdown files into components, but this is outside the scope of this plugin :)  
> Check out the [demo](https://rollup-plugin-pages.mazerty.fr) to see how it can be done with MDX.

## Parameters

There are actually 3 parameters that allow you to customize the behavior of the plugin:

* `absolutePath` _(string)_: absolute path to the pages' directory.
* `virtualModuleId` _(string, optional)_: name of the virtual module, defaults to `virtual:pages`.  
  Has to be unique, allows for multiple use of the plugin in the same project (i.e. for different directories).
* `filenamePattern` _(RegExp, optional)_: regular expression for the pages' filenames inside the directory, defaults to `/(?<date>.*)_(?<key>.*)\./` ([regexr](https://regexr.com/7p8gb)).  
  Only [named capturing groups](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Regular_expressions/Named_capturing_group) are considered valid, regular capturing groups will be ignored.

## Known issues

* [#1 - Hot update behaves quite erratically](https://gitlab.com/__mazerty__/rollup-plugin-pages/-/issues/1)
