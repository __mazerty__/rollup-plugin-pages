import {Dirent, promises as fs} from "fs"
import path from "path"
import yaml from "yaml"

/**
 * Rollup/Vite plugin that can be used to extract metadata from a directory of markdown files with frontmatter (such as blog pages).
 * It can then be imported like any other module in your existing application to populate a router or a summary page.
 * Optimized for lazy loading and code splitting, therefore hot update behavior can be erratic during development.
 *
 * @param absolutePath {string}
 *      absolute path to the pages' directory
 * @param virtualModuleId {string}
 *      name of the virtual module, defaults to "virtual:pages";
 *      has to be unique, allows for multiple use of the plugin in the same project (i.e. for different directories)
 * @param filenamePattern {RegExp}
 *      regular expression for the pages' filenames inside the directory, defaults to "/(?<date>.*)_(?<key>.*)\./" (matches "1970-01-01_lorem-ipsum.md" for example);
 *      only "named capturing groups" are considered valid, regular capturing groups will be ignored,
 *      see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Regular_expressions/Named_capturing_group for more information
 * @returns
 *      a plugin object whose structure is compatible with Rollup/Vite, in this case generating virtual modules,
 *      see https://rollupjs.org/plugin-development/#a-simple-example for more information
 */
export default (absolutePath, virtualModuleId = "virtual:pages", filenamePattern = /(?<date>.*)_(?<key>.*)\./) => {

    /**
     * Extracts the metadata from a page's filename and frontmatter
     *
     * @param page {Dirent}
     * @returns {Promise<{}>} the page's metadata
     */
    async function getMetadata(page) {
        const fullPath = path.join(page.path, page.name)
        const namedGroups = filenamePattern.exec(page.name).groups
        const fileContents = await fs.readFile(fullPath, "utf-8")
        const frontmatter = yaml.parseAllDocuments(fileContents)[0].toJSON()

        return {fullPath, ...namedGroups, ...frontmatter}
    }

    /**
     * Filters the pages from the directory's files and extracts their metadata
     *
     * @returns {Promise<{}[]>} the pages' metadata
     */
    async function getMetadataList() {
        const files = await fs.readdir(absolutePath, {withFileTypes: true})
        const pages = files.filter(entry => entry.isFile() && filenamePattern.test(entry.name))
        return Promise.all(pages.map(getMetadata))
    }

    /**
     * Extracts the pages' metadata, adds dynamic imports and generates the virtual module's code
     *
     * @returns {Promise<string>} the virtual module's code
     */
    async function getVirtualModuleCode() {
        return "export default " + JSON
            .stringify(await getMetadataList(), null, 2)
            // Here is the weird part: Vite is unable to do chunk splitting when dynamic imports points to a variable (like "() => import(page.fullPath)")
            // (see https://github.com/vitejs/vite/issues/14102 for more information)
            // so we need to put the import here, but since it's not a valid JSON we have to do it with string.replace()
            .replace(/"fullPath": "(.*?)"/g, '"import": () => import("$1")')
    }

    // Below are the Rollup plugin specifics, see https://rollupjs.org/plugin-development for more information
    const resolvedVirtualModuleId = "\0" + virtualModuleId
    return {
        name: virtualModuleId,
        resolveId: (moduleId) => moduleId === virtualModuleId ? resolvedVirtualModuleId : undefined,
        load: async (resolvedModuleId) => resolvedModuleId === resolvedVirtualModuleId ? getVirtualModuleCode() : undefined
    }
}
